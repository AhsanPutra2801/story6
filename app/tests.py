from django.test import TestCase,Client
from .models import DaftarKegiatan
from .views import index,buat_kegiatan,daftar_kegiatan,list_kegiatan
from django.urls import resolve
# Create your tests here.

class TestStory6(TestCase):
	def test_halaman_utama_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)

	def test_halaman_buat_kegiatan_exist(self):
		response = Client().get('/buat_kegiatan/')
		self.assertEqual(response.status_code,200)

	def test_halaman_daftar_exist(self):
		response = Client().get('/daftar_kegiatan/')
		self.assertEqual(response.status_code,200)

	def test_halaman_list_exist(self):
		response = Client().get('/list_kegiatan/')
		self.assertEqual(response.status_code,200)

	def test_func_halaman_utama(self):
		find = resolve('/')
		self.assertEqual(find.func,index)

	def test_func_halaman_buat_kegiatan(self):
		find = resolve('/buat_kegiatan/')
		self.assertEqual(find.func,buat_kegiatan)

	def test_func_halaman_daftar(self):
		find = resolve('/daftar_kegiatan/')
		self.assertEqual(find.func,daftar_kegiatan)

	def test_func_halaman_list_kegiatan(self):
		find = resolve('/list_kegiatan/')
		self.assertEqual(find.func,list_kegiatan)

	def test_model_daftar_kegiatan(self):
		infokegiatan = DaftarKegiatan.objects.create(namaKegiatan='kegiatan1',nama='nama1')
		testModel = DaftarKegiatan.objects.all().count()
		self.assertEqual(testModel,1)