from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
	path('',views.index,name='index'),
	path('buat_kegiatan/',views.buat_kegiatan,name='buat_kegiatan'),
	path('buatKegiatan/',views.buatKegiatan,name='buatKegiatan'),
	path('daftar_kegiatan/',views.daftar_kegiatan,name='daftar_kegiatan'),
	path('buatDaftar/',views.buatDaftar,name='buatDaftar'),
	path('list_kegiatan/',views.list_kegiatan,name='list_kegiatan'),
]