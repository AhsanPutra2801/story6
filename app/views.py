from django.shortcuts import render
from .models import DaftarKegiatan
# Create your views here.

def index(request):
	return render(request,'index.html')

def buat_kegiatan(request):
	return render(request,'buat_kegiatan.html')

def buatKegiatan(request):
	namaKegiatan = request.POST['namaKegiatan']
	infoKegiatan = DaftarKegiatan(namaKegiatan=namaKegiatan)
	infoKegiatan.save()
	return render(request,'buat_kegiatan.html')

def daftar_kegiatan(request):
	return render(request,'daftar_kegiatan.html')

# Buat daftar orang
def buatDaftar(request):
	nama = request.POST['nama']
	daftar = DaftarKegiatan(nama=nama)
	daftar.save()
	return render(request,'daftar_kegiatan.html')

def list_kegiatan(request):
	infoKegiatan = DaftarKegiatan.objects.all()
	kegiatan = {
		"info_kegiatan" : infoKegiatan
	}
	return render(request,'list_kegiatan.html',kegiatan)

